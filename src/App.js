import './App.css';
import React from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Network, Alchemy } from 'alchemy-sdk';




function App() {
  const [urlsOPENSEA, setURLS_OPENSEA] = useState([]);
  const [urlsALCHEMY, setURLS_ALCHEMY] = useState([]);
  const options_OPENSEA = {
    method: 'GET',
    url: `https://api.opensea.io/api/v1/assets?asset_contract_address=${getContractFromURL()}`,
    params: {order_direction: 'desc', limit: '20', include_orders: 'false'},
    headers: {accept: 'application/json', 'X-API-KEY': '701c059ef6ec43d39c920bcd07fa4ad9'}
  };
  // options alchemy
  const settings = {
    apiKey: "AY-5O78cFmA8ZQ1T9n9MJzQw3KoIdKdX", // Replace with your Alchemy API Key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);


  function getContractFromURL(req, adress){
    req = window.location.href.split('?')[1];
    adress = req.split('=')[1]
    return adress;
  }

  function requestNFTs_OPENSEA(){
    const fetchResult = async () => {
      await axios.request(options_OPENSEA).then(function (response){
        const tab = response.data.assets
        tab.forEach(element => {console.log(element.image_url);});
        setURLS_OPENSEA(tab);
      }).catch(function (error) {console.error(error);});
    };
    fetchResult();
  }

  function requestNFTs_ALCHEMY(){
    const req = async () => {
      await alchemy.nft.getNftsForContract(getContractFromURL()).then(function (response){
        const tab = response.nfts
        /*tab.forEach(element => {
          urlsALCHEMY8.push(element.rawMetadata.image);
        })*/
        setURLS_ALCHEMY(tab);
      }).catch(function (error) {console.error(error);});
    };
    req();
  }

  useEffect(() => {
    //requestNFTs_ALCHEMY();
    requestNFTs_OPENSEA();
  }, []);
  
  return (
    <div className="App">
      <h2>The most basic list of payment requests</h2>
      <div>
        {urlsALCHEMY.map((url) => {
          return (
            <span key={url.rawMetadata.image}>
              <img src={url.rawMetadata.image} alt="" width="25px"></img>
            </span>
          ); 
        })}  
      </div>
      <div>
        {urlsOPENSEA.map((url) => {
            return (
              <span key={url.image_url}>
                <img src={url.image_url} alt="" width="250px" hei></img>
              </span>
            ); 
          })} 
      </div>
    </div>
  );
}

export default App;
